'WhatTodo?' is a simple task management webapp.

Features of the app -

    1. You can add tasks and tags.
    
    2. You can Edit Tasks and tags.
    
    3. Mark Tasks as completed or, bookmark them.
    
    4. Delete completed tasks.
    
    5. Search for Tasks by tags.
    

Backend is built using Django/Python2.7/Sqlite3 with MVC architecture.

Frontend is simple with minimal CSS styling and html

Deployed on openshift at - http://todo-manjuhere.rhcloud.com/

