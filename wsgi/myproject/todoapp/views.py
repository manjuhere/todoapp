from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from .models import Task
from .forms import TaskForm, EditForm, SearchTask
import re

def processText(uStr):
    x = re.findall(r"[\w']+", uStr.encode('ascii', 'ignore'))
    x = list(set([item.strip().lower() for item in x]))
    text = filter(None, x)
    text = ', '.join(text)
    return text

def doesItExist(task_id):
    try:
        task = get_object_or_404(Task, pk=task_id)
    except :
        return 'no'

def index(request):
    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            task = Task(task_text=form.cleaned_data['enter_task'], tag_text=processText(form.cleaned_data['enter_tags']), entry_date=timezone.now(), bookmarked=form.cleaned_data['bookmark'])
            task.save()
            return HttpResponseRedirect('/')
    else:
        form = TaskForm()
    task_list = Task.objects.order_by('entry_date')
    bk_list = Task.objects.filter(bookmarked=True)
    return render(request, 'index.html', {
        'form': form,
        'task_list': task_list,
        'bk_list':bk_list
    })

def clear(request):
    Task.objects.all().delete()
    return HttpResponseRedirect('/')

def edit(request, task_id):
    if doesItExist(task_id)=='no':
        return HttpResponse("<center><h1><b>Error - 404</b></h1><br /><h3>Page Does not exist.</h3><br /> <a href=""/""> Go Back </a></center>")
    task = get_object_or_404(Task, pk=task_id)
    if request.method == 'POST':
        form = EditForm(request.POST)
        if form.is_valid():
            if form.cleaned_data['enter_task'] != '':
                task.task_text = form.cleaned_data['enter_task']
            if form.cleaned_data['enter_tags'] != '':
                task.tag_text = processText(form.cleaned_data['enter_tags'])
            task.save()
            return HttpResponseRedirect('/')
    else:
        form = EditForm()
    return render(request, 'edit.html', {'form':form, 'task':task})

def completed(request, task_id):
    if doesItExist(task_id)=='no':
        return HttpResponse("<center><h1><b>Error - 404</b></h1><br /><h3>Page Does not exist.</h3><br /> <a href=""/""> Go Back </a></center>")
    task = get_object_or_404(Task, pk=task_id)
    if task.done==True:
        task.done=False
    else:
        task.done=True
    task.save()
    return HttpResponseRedirect('/')

def bookmark(request, task_id):
    if doesItExist(task_id)=='no':
        return HttpResponse("<center><h1><b>Error - 404</b></h1><br /><h3>Page Does not exist.</h3><br /> <a href=""/""> Go Back </a></center>")
    task = get_object_or_404(Task, pk=task_id)
    if task.bookmarked==True:
        task.bookmarked=False
    else:
        task.bookmarked=True
    task.save()
    return HttpResponseRedirect('/')

def delete(request, task_id):
    task = get_object_or_404(Task, pk=task_id)
    task.delete()
    return HttpResponseRedirect('/')

def search(request):
    all_tags = {}
    tags = Task.objects.all()
    for i in range(Task.objects.all().count()):
        all_tags[tags[i].id] = tags[i].tag_text.encode('ascii', 'ignore').split(", ")
    full_list = list(set([item.strip() for sublist in all_tags.values() for item in sublist]))
    if request.method == "POST":
        form = SearchTask(request.POST)
        if form.is_valid():
            resultFound = []
            key = re.findall(r"[\w']+", form.cleaned_data['search_task'].encode('ascii', 'ignore'))
            key = list(set([item.strip().lower() for item in key]))
            for i in key:
                for j in all_tags.keys():
                    if i in all_tags[j]:
                        resultFound.append(j)
            result = Task.objects.filter(id__in=resultFound)
            return render(request, 'search.html', {'form': form, 'alltags':full_list, 'resultFound':result, 'method':"POST"})
    else:
        form = SearchTask()
    return render(request, 'search.html', {'form': form, 'alltags':full_list, 'method':"GET"})
