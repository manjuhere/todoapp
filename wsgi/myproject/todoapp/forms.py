from django import forms

class TaskForm(forms.Form):
    enter_task = forms.CharField(label='Task', max_length=200, widget=forms.TextInput(attrs={'placeholder': 'Enter the Task', 'size':'55'}))
    enter_tags = forms.CharField(label='Tags', max_length=50, widget=forms.TextInput(attrs={'placeholder': 'Enter Tags separated by space', 'size':'55'}))
    bookmark = forms.BooleanField(initial=False, required=False)

class EditForm(forms.Form):
    enter_task = forms.CharField(label='Task', max_length=200, required=False, widget=forms.TextInput(attrs={'placeholder': 'Enter Task | Leave blank to not modify', 'size':'55'}))
    enter_tags = forms.CharField(label='Tags', max_length=50, required=False, widget=forms.TextInput(attrs={'placeholder': 'Tags | Leave blank to not modify', 'size':'55'}))

class SearchTask(forms.Form):
    search_task = forms.CharField(label='Enter Tags', max_length=200, required=False, widget=forms.TextInput(attrs={'placeholder': 'Enter the Tag/s', 'size':'45'}))
