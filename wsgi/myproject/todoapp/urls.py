from django.conf.urls import url

from . import views

app_name='todo'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^clear/', views.clear, name='clear'),
    url(r'^edit/(?P<task_id>[0-9]+)', views.edit, name='edit'),
    url(r'^completed/(?P<task_id>[0-9]+)', views.completed, name='completed'),
    url(r'^delete/(?P<task_id>[0-9]+)', views.delete, name='delete'),    
    url(r'^bookmark/(?P<task_id>[0-9]+)', views.bookmark, name='bookmark'),
    url(r'^search/', views.search, name='search'),
]
