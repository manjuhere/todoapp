from __future__ import unicode_literals
from django.db import models

# Create your models here.
class Task(models.Model):
    task_text = models.CharField(max_length=200)
    tag_text = models.CharField(max_length=200, default="NULL")
    entry_date = models.DateTimeField('date entered')
    done = models.BooleanField(default=False)
    bookmarked = models.BooleanField(default=False)
    def __str__(self):
        desc = "Task - " + self.task_text + ".\nTags - " + self.tag_text + ".\n"
        return desc
